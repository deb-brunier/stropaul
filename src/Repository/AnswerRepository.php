<?php

namespace App\Repository;

use App\Entity\Answer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Answer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Answer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Answer[]    findAll()
 * @method Answer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Answer::class);
    }

    /**
     * @return Answer[] Returns an array of Answer objects
     */
    
    // chercher dans les réponses du poll courant si l'utilisateur courant à déjà voté
    // utilisé dans la public fonction vote dans le PollController
    public function hasVoted($idPoll, $idUser)
    {
        $qb = $this->createQueryBuilder('a')
            ->andWhere('a.question = :idP')
            ->join('a.users', 'u')
            ->andWhere('u = :idU')
            ->setParameter('idU', $idUser)
            ->setParameter('idP', $idPoll)
        ;
        return $qb->getQuery()->getResult();
    }

    /*

    
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Answer
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

}
