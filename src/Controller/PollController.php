<?php

namespace App\Controller;

use App\Entity\Answer;
use App\Entity\Poll;
use App\Form\PollType;
use App\Form\VoteType;
use App\Repository\PollRepository;
use App\Repository\AnswerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/poll")
 */
class PollController extends AbstractController
{
    /**
     * @Route("/", name="poll_index", methods={"GET"})
     */
    public function index(PollRepository $pollRepository, EntityManagerInterface $entityManager): Response
    {
        $loggedUser = $this->getUser();
        $pollsCreated = $entityManager->getRepository(Poll::class)->pollCreatedByUser($this->getUser());
        $pollsVoted = $entityManager->getRepository(Poll::class)->pollVotedInByUser($this->getUser());
        $otherPolls = $entityManager->getRepository(Poll::class)->otherPublishedPolls($this->getUser());
        return $this->render('poll/index.html.twig', [
            'pollsCreated' => $pollsCreated,
            'pollsVoted' => $pollsVoted,
            'otherPolls' => $otherPolls,
            'polls' => $pollRepository->findAll(),
            'thisUser' => $loggedUser,
        ]);
    }

    /**
     * @Route("/new", name="poll_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $poll = new Poll();
        $poll->setUser($this->getUser());

        $answer = new Answer();
        $poll->addAnswer($answer);

        $answer = new Answer();
        $poll->addAnswer($answer);

        $form = $this->createForm(PollType::class, $poll);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($poll);
            $entityManager->flush();

            return $this->redirectToRoute('poll_show', ['id' => $poll->getId()]);
        }

        return $this->render('poll/new.html.twig', [
            'poll' => $poll,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}", name="poll_show", methods={"GET"})
     */
    public function show(Poll $poll): Response
    {
        $loggedUser = $this->getUser();

        return $this->render('poll/show.html.twig', [
            'thisUser' => $loggedUser,
            'poll' => $poll,
        ]);
    }

    /**
     * @Route("/{id}/vote", name="poll_vote", methods={"GET","POST"})
     */
    public function vote(Poll $poll, $id, Request $request, EntityManagerInterface $entityManager): Response
    {
        $pollVote = $entityManager->getRepository(Poll::class)->find($id); // on va chercher l'id du poll
        // ici on met null dans le deuxième paramètre car on ne veut pas le lier à une entité (on a mis mapped false dans le formtype)
        $form = $this->createForm(VoteType::class, null, [
            'answers' => $pollVote->getAnswers(),
            'multiple' => $poll-> getType(),
        ]);
        $form->handleRequest($request);

        // vérifie si l'user connecté à déjà voté pour une réponse du sondage courant
        $hasVoted = $entityManager->getRepository(Answer::class)->hasVoted($poll->getId(), $this->getUser());
        $multiple = $poll->getType();

        if ($form->isSubmitted() && $form->isValid()) {

            // si l'utilisateur n'a pas encore voté !hasVoted, on persist et flush sa réponse
            if (!$hasVoted) {
                if ($multiple === true) {
                    $selectedAnswers = $form['answers']->getData(); // ici answers fait réf au add 'answers' du choicetype dans le formtype
                
                    foreach ($selectedAnswers as $selectedAnswer) {
                        $selectedAnswer->addUser($this->getUser());
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($selectedAnswer);
                    }
                }
                else {
                    $selectedAnswer = $form['answers']->getData();
                    $selectedAnswer->addUser($this->getUser());
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($selectedAnswer);
                }

                $entityManager->flush();
                return $this->redirectToRoute('poll_show', ['id' => $poll->getId()]);
            }

            else {
                $this->addFlash(
                    'notice',
                    'Vous avez déjà voté à ce sondage!'
                );
            }

        }

        return $this->render('poll/vote.html.twig', [
            'multiple' => $multiple,
            'poll' => $poll,
            'answer' => $pollVote->getAnswers(),
            'voteForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="poll_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Poll $poll): Response
    {
        if ($poll->getUser() === $this->getUser()) {
            $form = $this->createForm(PollType::class, $poll);
            $form->handleRequest($request);
    
            if ($form->isSubmitted() && $form->isValid()) {
                $this->getDoctrine()->getManager()->flush();
    
                return $this->redirectToRoute('poll_show', ['id' => $poll->getId()]);
            }
    
            return $this->render('poll/edit.html.twig', [
                'poll' => $poll,
                'form' => $form->createView(),
            ]);
        }

        else {
            throw $this->createAccessDeniedException();
        }

    }

    /**
     * @Route("/{id}", name="poll_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Poll $poll): Response
    {
        if ($poll->getUser() === $this->getUser()) {
            if ($this->isCsrfTokenValid('delete'.$poll->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($poll);
                $entityManager->flush();
            }
            return $this->redirectToRoute('poll_new');
        }
        else {
            throw $this->createAccessDeniedException();
        }
    }
}
