<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* poll/index.html.twig */
class __TwigTemplate_8e32ca53d7c731747c40ba8854f398ff0fe23b71fa6e2957598fa2cbd9bc9835 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "poll/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "poll/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "poll/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Sondage";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <h1>Sondage</h1>

    ";
        // line 9
        echo "    <h2>Sondages créées</h2>
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pollsCreated"]) || array_key_exists("pollsCreated", $context) ? $context["pollsCreated"] : (function () { throw new RuntimeError('Variable "pollsCreated" does not exist.', 10, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["poll"]) {
            // line 11
            echo "            <div class=\"indexBlock\">
                <div>
                    <h3>";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["poll"], "question", [], "any", false, false, false, 13), "html", null, true);
            echo "</h3>
                    ";
            // line 14
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["poll"], "status", [], "any", false, false, false, 14), false))) {
                // line 15
                echo "                        <p>enregistré comme brouillon</p>
                    ";
            }
            // line 17
            echo "                </div>
                <div class=\"btnBlock\">
                        <button><a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["poll"], "id", [], "any", false, false, false, 19)]), "html", null, true);
            echo "\">modifier</a></button>
                        <button><a href=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_show", ["id" => twig_get_attribute($this->env, $this->source, $context["poll"], "id", [], "any", false, false, false, 20)]), "html", null, true);
            echo "\">résultats</a></button>
                        <button><a href=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_vote", ["id" => twig_get_attribute($this->env, $this->source, $context["poll"], "id", [], "any", false, false, false, 21)]), "html", null, true);
            echo "\">voter</a></button>
                </div>
            </div>

        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 26
            echo "            <div><p>Aucun sondage créée.</p></div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['poll'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "
    ";
        // line 30
        echo "    <h2>Sondage pour lesquels vous avez voté</h2>
        ";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pollsVoted"]) || array_key_exists("pollsVoted", $context) ? $context["pollsVoted"] : (function () { throw new RuntimeError('Variable "pollsVoted" does not exist.', 31, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["poll"]) {
            // line 32
            echo "            <div class=\"indexBlock\">
                <div>
                    <h3>";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["poll"], "question", [], "any", false, false, false, 34), "html", null, true);
            echo "</h3>
                </div>
                <div class=\"btnBlock\">
                        ";
            // line 37
            if ((twig_get_attribute($this->env, $this->source, $context["poll"], "user", [], "any", false, false, false, 37) === (isset($context["thisUser"]) || array_key_exists("thisUser", $context) ? $context["thisUser"] : (function () { throw new RuntimeError('Variable "thisUser" does not exist.', 37, $this->source); })()))) {
                // line 38
                echo "                            <button><a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["poll"], "id", [], "any", false, false, false, 38)]), "html", null, true);
                echo "\">modifier</a></button>
                        ";
            }
            // line 40
            echo "                        <button><a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_show", ["id" => twig_get_attribute($this->env, $this->source, $context["poll"], "id", [], "any", false, false, false, 40)]), "html", null, true);
            echo "\">résultats</a></button>
                </div>
            </div>

        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 45
            echo "            <div><p>Aucun vote dans un sondage</p></div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['poll'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "

    ";
        // line 50
        echo "    <h2>Autres sondages</h2>

        ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["otherPolls"]) || array_key_exists("otherPolls", $context) ? $context["otherPolls"] : (function () { throw new RuntimeError('Variable "otherPolls" does not exist.', 52, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["poll"]) {
            // line 53
            echo "            <div class=\"indexBlock\">
                <div>
                    <h3>";
            // line 55
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["poll"], "question", [], "any", false, false, false, 55), "html", null, true);
            echo "</h3>
                </div>
                <div class=\"btnBlock\">
                    ";
            // line 58
            if ((twig_get_attribute($this->env, $this->source, $context["poll"], "user", [], "any", false, false, false, 58) === (isset($context["thisUser"]) || array_key_exists("thisUser", $context) ? $context["thisUser"] : (function () { throw new RuntimeError('Variable "thisUser" does not exist.', 58, $this->source); })()))) {
                // line 59
                echo "                        <button><a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["poll"], "id", [], "any", false, false, false, 59)]), "html", null, true);
                echo "\">modifier</a></button>
                    ";
            }
            // line 61
            echo "                        <button><a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_show", ["id" => twig_get_attribute($this->env, $this->source, $context["poll"], "id", [], "any", false, false, false, 61)]), "html", null, true);
            echo "\">résultats</a></button>
                        <button><a href=\"";
            // line 62
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_vote", ["id" => twig_get_attribute($this->env, $this->source, $context["poll"], "id", [], "any", false, false, false, 62)]), "html", null, true);
            echo "\">voter</a></button>
                </div>
            </div>

        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 67
            echo "            <div><p>Aucun autre sondage</p></div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['poll'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 69
        echo "
    <button><a href=\"";
        // line 70
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_new");
        echo "\">créer un sondage</a></button>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "poll/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  246 => 70,  243 => 69,  236 => 67,  226 => 62,  221 => 61,  215 => 59,  213 => 58,  207 => 55,  203 => 53,  198 => 52,  194 => 50,  190 => 47,  183 => 45,  172 => 40,  166 => 38,  164 => 37,  158 => 34,  154 => 32,  149 => 31,  146 => 30,  143 => 28,  136 => 26,  126 => 21,  122 => 20,  118 => 19,  114 => 17,  110 => 15,  108 => 14,  104 => 13,  100 => 11,  95 => 10,  92 => 9,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Sondage{% endblock %}

{% block body %}
    <h1>Sondage</h1>

    {# polls créés par l'utilisateur connecté #}
    <h2>Sondages créées</h2>
        {% for poll in pollsCreated %}
            <div class=\"indexBlock\">
                <div>
                    <h3>{{ poll.question }}</h3>
                    {% if poll.status == false %}
                        <p>enregistré comme brouillon</p>
                    {% endif %}
                </div>
                <div class=\"btnBlock\">
                        <button><a href=\"{{ path('poll_edit', {'id': poll.id}) }}\">modifier</a></button>
                        <button><a href=\"{{ path('poll_show', {'id': poll.id}) }}\">résultats</a></button>
                        <button><a href=\"{{ path('poll_vote', {'id': poll.id}) }}\">voter</a></button>
                </div>
            </div>

        {% else %}
            <div><p>Aucun sondage créée.</p></div>
        {% endfor %}

    {# polls dans lesquels l'utilisateur connecté a voté #}
    <h2>Sondage pour lesquels vous avez voté</h2>
        {% for poll in pollsVoted %}
            <div class=\"indexBlock\">
                <div>
                    <h3>{{ poll.question }}</h3>
                </div>
                <div class=\"btnBlock\">
                        {% if poll.user is same as(thisUser) %}
                            <button><a href=\"{{ path('poll_edit', {'id': poll.id}) }}\">modifier</a></button>
                        {% endif %}
                        <button><a href=\"{{ path('poll_show', {'id': poll.id}) }}\">résultats</a></button>
                </div>
            </div>

        {% else %}
            <div><p>Aucun vote dans un sondage</p></div>
        {% endfor %}


    {# tous les polls du site #}
    <h2>Autres sondages</h2>

        {% for poll in otherPolls %}
            <div class=\"indexBlock\">
                <div>
                    <h3>{{ poll.question }}</h3>
                </div>
                <div class=\"btnBlock\">
                    {% if poll.user is same as(thisUser) %}
                        <button><a href=\"{{ path('poll_edit', {'id': poll.id}) }}\">modifier</a></button>
                    {% endif %}
                        <button><a href=\"{{ path('poll_show', {'id': poll.id}) }}\">résultats</a></button>
                        <button><a href=\"{{ path('poll_vote', {'id': poll.id}) }}\">voter</a></button>
                </div>
            </div>

        {% else %}
            <div><p>Aucun autre sondage</p></div>
        {% endfor %}

    <button><a href=\"{{ path('poll_new') }}\">créer un sondage</a></button>
{% endblock %}
", "poll/index.html.twig", "/Users/vikki/Documents/SIMPLON/dev/tp9-stopaul3/templates/poll/index.html.twig");
    }
}
